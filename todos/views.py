from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm


# Create your views here.
def todo_list_view(request):
    lists = TodoList.objects.all()
    context = {
        "todoLists": lists,
    }
    return render(request, "todos/lists.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todoList": list,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()
        content = {"list_form": form}
        return render(request, "todos/create.html", content)


def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm(instance=list)
        content = {"list_form": form}
        return render(request, "todos/edit.html", content)


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            listid = form.cleaned_data["list"]
            form.save()
            return redirect("todo_list_detail", id=listid.id)
    else:
        form = ItemForm()
        content = {"item_form": form}
        return render(request, "todos/newitem.html", content)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            listid = form.cleaned_data["list"]
            list = form.save()
            return redirect("todo_list_detail", id=listid.id)
    else:
        form = ItemForm(instance=item)
        content = {"item_form": form}
        return render(request, "todos/edititem.html", content)
